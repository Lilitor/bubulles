const counterDisplay = document.querySelector("h3");
const readme = document.querySelector("h2");
let counter = 0;

const bubbleMaker = () => {
  const bubble = document.createElement("span");
  bubble.classList.add("bubble");
  document.body.appendChild(bubble);

  const size = Math.random() * 200 + 100 + "px";
  bubble.style.height = size;
  bubble.style.width = size;

  bubble.style.top = Math.random() * 100 + 50 + "%";
  bubble.style.left = Math.random() * 100 + "%";

  const plusMinus = Math.random() > 0.5 ? 1 : -1;
  bubble.style.setProperty("--left", Math.random() * 100 * plusMinus + "%");

  bubble.addEventListener("click", () => {
    counter++;
    counterDisplay.textContent = counter;
    bubble.remove();
  });

  setTimeout(() => {
    bubble.remove();
  }, 8000);
};

const pentaMaker = () => {
  const penta = document.createElement("div");
  penta.classList.add("penta");
  document.body.appendChild(penta);
  
  const size = Math.random() * 200 + 100 + "px";
  penta.style.height = size;
  penta.style.width = size;
  
  penta.style.top = Math.random() * 100 + 50 + "%";
  penta.style.left = Math.random() * 100 + "%";
  
  const plusMinus = Math.random() > 0.5 ? 1 : -1;
  penta.style.setProperty("--left", Math.random() * 100 * plusMinus + "%");
  
  penta.addEventListener("click", () => {
    counter--;
    counterDisplay.textContent = counter;
    penta.remove();
  });
  
  setTimeout(() => {
    penta.remove();
  }, 8000);
};

window.addEventListener("load", () => {
  const throwb = setInterval(bubbleMaker, 300);
  const throwp = setInterval(pentaMaker, 500);
  setTimeout(() => {
    clearInterval(throwb);
    clearInterval(throwp);
    counterDisplay.style.fontSize = "5rem";
    counterDisplay.textContent = "Vous avez marqué " + counter + " points";
  }, 60000);
});

setTimeout(() => {
  readme.remove();
}, 500);